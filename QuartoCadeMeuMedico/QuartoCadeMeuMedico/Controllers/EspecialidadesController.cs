﻿using QuartoCadeMeuMedico.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuartoCadeMeuMedico.Controllers
{
    public class EspecialidadesController : Controller
    {
        private EntidadesCadeMeuMedicoBD db =
           new EntidadesCadeMeuMedicoBD();
        // GET: Especialidades
        public ActionResult Index()
        {
            var especialidade = db.Especialidades.ToList();
            return View(especialidade);
            
        }
        public ActionResult Adicionar()
        {
            
            return View();
        }


        [HttpPost]
        public ActionResult Adicionar(Especialidades especialidade)
        {
            if (ModelState.IsValid)
            {
                db.Especialidades.Add(especialidade);
                db.SaveChanges();
                Console.WriteLine("Persistiu!!!");
                return RedirectToAction("Index");
            }
            
            return View(especialidade);
        }
    }
}