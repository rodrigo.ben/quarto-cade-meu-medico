﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuartoCadeMeuMedico.Models;

namespace QuartoCadeMeuMedico.Controllers
{
    public class MedicosController : Controller
    {
        private EntidadesCadeMeuMedicoBD db =
            new EntidadesCadeMeuMedicoBD();
        // GET: Medicos
        public ActionResult Index()
        {
            var medicos = db.Medicos.Include("Cidades")
                .Include("Especialidades").ToList();
            return View(medicos);
        }
        public ActionResult Adicionar()
        {
            ViewBag.IDCidade =
                new SelectList(db.Cidades, 
                "IDCidade",
                "Nome");
            ViewBag.IDEspecialidade = new SelectList(db.Especialidades, 
                "IDEspecialidade",
                "Nome");
            return View();
        }

        
        [HttpPost]
        public ActionResult Adicionar(Medicos medico)
        {
            if(ModelState.IsValid)
            {
                db.Medicos.Add(medico);
                db.SaveChanges();
                Console.WriteLine("Persistiu!!!");
                return RedirectToAction("Index");
            }

            ViewBag.IDCidade = new SelectList(db.Cidades, "IDCidade",
                "Nome", medico.IDCidade);
            ViewBag.IDEspecialidade =
                new SelectList(db.Especialidades,
                "IDEspecialidade", "Nome", medico.IDEspecialidade);
                return View(medico);
        }
     

        public ActionResult Editar(long id)
        {
            Medicos medico = db.Medicos.Find(id);
            /*
            ViewBag.IDCidade = new SelectList(db.Especialidades,
                "IDEspecialidade", "Nome", medico.IDEspecialidade);
                */
            ViewBag.IDCidade = new SelectList(db.Cidades, "IDCidade", "Nome", medico.IDCidade);
            ViewBag.IDEspecialidade = new SelectList(db.Especialidades, "IDEspecialidade", "Nome", medico.IDEspecialidade);

            return View(medico);
        }

        [HttpPost]
        public ActionResult Editar(Medicos medico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medico).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDEspecialidade = new SelectList(db.Cidades, "IDCidade",
                "Nome",
                medico.IDCidade);
            ViewBag.IDEspecialidade = new SelectList(db.Especialidades,
                "IDEspecialidade", "Nome", medico.IDEspecialidade);
            return View(medico);
        }


        [HttpPost]
        public string Excluir(long id)
        {
            try
            {
                Medicos medico = db.Medicos.Find(id);
                db.Medicos.Remove(medico);
                db.SaveChanges();
                return Boolean.TrueString;
            }
            catch
            {
                return Boolean.FalseString;
            }


        }

     }
}