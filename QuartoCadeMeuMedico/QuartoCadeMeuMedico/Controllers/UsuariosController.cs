﻿using QuartoCadeMeuMedico.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuartoCadeMeuMedico.Controllers
{
    public class UsuariosController : Controller
    {
        private EntidadesCadeMeuMedicoBD db =
            new EntidadesCadeMeuMedicoBD();
        // GET: Usuarios
        public ActionResult Index()
        {
            var usuario = db.Usuarios.ToList();
            return View(usuario);
            return View();
        }
        public ActionResult Adicionar()
        {
           
            return View();
        }


        [HttpPost]
        public ActionResult Adicionar(Usuarios usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
                Console.WriteLine("Persistiu!!!");
                return RedirectToAction("Index");
            }

            
            return View(usuario);
        }
    }
}