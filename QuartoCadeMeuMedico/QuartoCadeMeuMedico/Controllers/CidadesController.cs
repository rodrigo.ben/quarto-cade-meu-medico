﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuartoCadeMeuMedico.Models;

namespace QuartoCadeMeuMedico.Controllers
{
    public class CidadesController : Controller
    {
        private EntidadesCadeMeuMedicoBD bd =
            new EntidadesCadeMeuMedicoBD();
        // GET: Cidades
        public ActionResult Index()
        {
            var cidade = bd.Cidades.ToList();
            return View(cidade);
        }
        
        public ActionResult Adicionar()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Adicionar(Cidades cidade)
        {
            if(ModelState.IsValid)
            bd.Cidades.Add(cidade);
            bd.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}